import os

import google.auth
import mock
from google.cloud import firestore

if "FIRESTORE_HOST" in os.environ:
    host = os.environ["FIRESTORE_HOST"]
else:
    host = "localhost"

os.environ["FIRESTORE_DATASET"] = "test"
os.environ["FIRESTORE_EMULATOR_HOST"] = f"{host}:9090"
os.environ["FIRESTORE_EMULATOR_HOST_PATH"] = f"{host}:9090/firestore"
os.environ["FIRESTORE_HOST"] = f"http://{host}:9090"
os.environ["FIRESTORE_PROJECT_ID"] = "test"
credentials = mock.Mock(spec=google.auth.credentials.Credentials)
db = firestore.Client(project="test", credentials=credentials)


def main():
    doc_ref = db.collection("garage").document("item")
    doc_ref.set({"foo": "bar"})

    for doc in db.collection("garage").stream():
        print(doc.to_dict())


if __name__ == "__main__":
    main()
